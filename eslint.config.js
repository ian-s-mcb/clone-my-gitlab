import globals from 'globals'
import prettier from 'eslint-plugin-prettier'
import js from '@eslint/js'

export default [
  js.configs.recommended,
  {
    files: ['**/*.{js,jsx,mjs,cjs,ts,tsx}'],
    plugins: {
      prettier,
    },
    languageOptions: {
      globals: { ...globals.browser },
    },
    rules: {
      'prettier/prettier': 'warn',
      'no-debugger': 'off',
    },
  },
]
