# clone-my-gitlab

Clones all repos tied to your GitLab account, so you can sleep better at night.

### Works as follows
- the user
  - creates a [Personal Access Token][token] with "read_api" scope
  - creates a copy of .env.example called .env.local and places the token
    inside
  - runs `npm run start -- myrepos` where "myrepos" is the folder on the user's
    computer where they want the repos placed
- the script
  - retrieves the URL's of all repos associated with the user via GitLab's
    [GraphQL API][api] and the NPM library `graphql-request`
  - speedily executes git commands to clone all repo using non-blocking, child
    processes via Node.js

### Assumptions
- the script is non-interactive and won't tell you how large all your repos are
  before attempting to clone them. Be patient if you have hundreds of repos
  or a slow Internet connection. For reference: cloning 80+ repos with an
  overview filesize of 275 MiB took under 30 seconds on my home connection.
- Git must be installed
- SSH keys are in place to communicate with GitLab's repo hosting service
- works on Node v18.16.0
- only tested on Linux, so use on Windows might require some troubleshooting

[api]: https://docs.gitlab.com/ee/api/graphql/
[token]: https://gitlab.com/-/profile/personal_access_tokens
