import { exec } from 'node:child_process'
import { mkdirSync } from 'node:fs'
import { argv, chdir, env } from 'node:process'

import * as dotenv from 'dotenv'
import { gql, GraphQLClient } from 'graphql-request'

async function collectRepoUrls(endpoint, token) {
  console.log('Collecting repo URLS...')
  const gqlClient = createGraphQLClient(token, endpoint)
  const {
    projects: { edges: repos },
  } = await gqlClient.request(
    gql`
      {
        projects(membership: true) {
          edges {
            node {
              sshUrlToRepo
            }
          }
        }
      }
    `
  )
  if (!repos.length) {
    throw new Error('no repos found')
  }
  console.log(`Found ${repos.length} repos.`)
  return repos
}

function createGraphQLClient(token, endpoint) {
  const client = new GraphQLClient(endpoint, {
    headers: {
      authorization: `Bearer ${token}`,
    },
  })

  return client
}

async function main() {
  try {
    // Read dotenv for API token
    dotenv.config({ path: './.env.local' })
    const token = env.GRAPHQL_TOKEN
    const endpoint = 'https://gitlab.com/api/graphql'
    if (!token) {
      throw new Error('expected API token in dotenv file')
    }

    // Check CLI arguments
    if (argv.length < 3) {
      throw new Error('expected destination folder argument')
    }

    // Make destination dir, cd into it
    const dest = argv[2]
    mkdirSync(dest, { recursive: true })
    chdir(dest)

    const repos = await collectRepoUrls(endpoint, token)

    // Clone repos
    console.log(`Cloning repos to ${dest}...`)
    repos.forEach(({ node: repo }) => {
      exec(`"git" clone ${repo.sshUrlToRepo}`)
    })
  } catch (err) {
    console.log('Error', err.message)
  }
}
main()
